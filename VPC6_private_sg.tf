resource "aws_security_group" "private_sg" {
    name = "${var.environment}-private-sg"
    description = "private ssh"

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["10.0.0.0/16"]
    }
    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["10.0.0.0/16"]
    }
    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
    vpc_id = "${aws_vpc.vpc.id}"

    tags {
      Name        = "${var.environment}-private-sg"
      Environment = "${var.environment}"
    }
}
